# Modbus Library
Multiple projects for Modbus RTU/ASCII/TCP library. See individual project readme files.
Targets .NET Standard 2.1. Written in C# for Microsoft Visual Studio 2022.

## Sub-Projects
Inetkid.Modbus: The base framework for Modbus functionalities.

Inetkid.Hashing.Crc16: Provides non-cryptographic Crc16 hashing function.

Inetkid.Hashing.Lrc: Provides non-cryptographic Lrc hashing function.

Inetkid.Modbus.Rtu: Modbus RTU extension for Inetkid.Modbus.

Inetkid.Modbus.Ascii: Modbus ASCII extension for Inetkid.Modbus.

Inetkid.Modbus.Tcp: Modbus TCP extension for Inetkid.Modbus.

Inetkid.Modbus.Server: Modbus server/slave implementation.

## License
Copyright 2023 Raymond Tan.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. 
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
