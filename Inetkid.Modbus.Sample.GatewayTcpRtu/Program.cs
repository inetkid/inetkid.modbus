﻿using System.Net;
using System.Net.Sockets;
using System.IO.Ports;
using Inetkid.Modbus.Model;
using Inetkid.Modbus.Serialization;

// Initialize the Tcp server to listen to port 502
TcpListener listener = new(IPAddress.Any, 502);

// Initialize the serial port to COM1
using SerialPort sp = new("COM1") { ReadTimeout = 500, WriteTimeout = 100 };

// Flag to stop listening and exit program
bool fStop = false;

// Listen to Ctrl-C
Console.CancelKeyPress += delegate {
    fStop = true;
};

// Open the serial port
sp.Open();

// Start listening to the Tcp port
listener.Start();

// Get the serial stream
using Stream rtuStream = sp.BaseStream;

// Setup mutex that enforce single thread to read/write to the serial port
using SemaphoreSlim mutex = new(1);

while (!fStop)
{
    while (listener.Pending())
        listener.AcceptTcpClientAsync().ContinueWith(async task =>
            {
                // Accept incoming connection and get the network stream
                using TcpClient client = task.Result;
                using NetworkStream tcpStream = client.GetStream();

                // Initialize the serializer
                ModbusTcpFormatter<ModbusFrameModel> tcpFormatter = new(ModbusTcpFormatter<ModbusFrameModel>.ModbusRoles.Server);
                ModbusRtuFormatter<ModbusFrameModel> rtuFormatter = new(ModbusRtuFormatter<ModbusFrameModel>.ModbusRoles.Master);

                // loop until tcp connection idle for 5 minutes
                for (int idleCounter = 0; idleCounter < 3000; idleCounter++)
                {
                    while (tcpStream.DataAvailable)
                    {
                        idleCounter = 0;

                        // Locking critical section
                        await mutex.WaitAsync().ConfigureAwait(false);

                        try
                        {
                            // Clear the serial stream
                            while (sp.BytesToRead > 0)
                                rtuStream.ReadByte();

                            // Copy the Tcp frame to the Rtu stream
                            // rtuFormatter.Serialize(rtuStream, tcpFormatter.Deserialize(tcpStream));

                            // Allow 500ms for the slave to response
                            await Task.Delay(500);

                            // Copy the Tcp frame to the Rtu stream
                            // if (sp.BytesToRead > 0)
                            // tcpFormatter.Serialize(tcpStream, rtuFormatter.Deserialize(rtuStream)));
                        }
                        finally
                        {
                            // Releasing the lock
                            mutex.Release();
                        }
                    }
                    await Task.Delay(100);
                }
            });
    Thread.Sleep(31);
}