# CRC16 Hash Stream
On the fly System.IO.Stream generation of Cyclic Redundancy Check 16 bits for .net 6.0.
Target .NET Standard 2.1. Written in C# for Microsoft Visual Studio 2022.

## Installing
```
dotnet add package Inetkid.IO.Hashing.Crc16
```

## Usage
```
byte[] buffer = { 1, 2, 3 };
System.IO.MemoryStream ms = new System.IO.MemoryStream();
Inetkid.IO.Hashing.Crc16 outStream = new Inetkid.IO.Hashing.Crc16(ms);
outStream.Write(buffer, 0, buffer.length);
Console.WriteLine(outStream.WriteCrc);
```

## License
Copyright 2023 Raymond Tan.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. 
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

## Contributing
You are welcomed to improve the code. Please visit 
    https://gitlab.com/inetkid/inetkid.modbus

Targets .net 6 and above. Written in C# for Microsoft Visual Studio 2022.
