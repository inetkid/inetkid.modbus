﻿using Inetkid.IO.Hashing;
using Inetkid.Modbus.Text;
using System.Text;

namespace Inetkid.Modbus.Serialization
{
    /// <summary>
    /// Base class for Modbus ASCII serialization.
    /// </summary>
    public abstract class ModbusAsciiFormatter : ModbusFormatter
    {
        /// <summary>
        /// Modbus role as a Modbus Master (Host) or a Modbus Slave (Device).
        /// </summary>
        public enum ModbusRoles { Master, Slave };

        /// <summary>
        /// Get or set the role as a Modbus Master (Host) or a Modbus Slave (Device).
        /// </summary>
        public virtual ModbusRoles Role { get; set; }
    }

    /// <summary>
    /// Serialize and deserialize modbus frame from Modbus Ascii serial stream.
    /// </summary>
    /// <typeparam name="T">A type that hold structured modbus frame and implements the IModbusSerializable.</typeparam>
    public class ModbusAsciiFormatter<T> : ModbusAsciiFormatter where T : IModbusSerializable, new()
    {
        /// <summary>
        /// Initialize modbus formatter with the default role of a Modbus Master (Host).
        /// </summary>
        public ModbusAsciiFormatter() : this(ModbusRoles.Master) { }

        /// <summary>
        /// Initialize modbus formatter with the specified role.
        /// </summary>
        /// <param name="role">Specify the role of a Modbus Master (Host) or a Modbus Slave (Device).</param>
        public ModbusAsciiFormatter(ModbusRoles role) => Role = role;

        /// <summary>
        /// Deserialize modbus frame from the serial stream.
        /// </summary>
        /// <param name="serializationStream">The System.IO.Stream to deserialize.</param>
        /// <returns>>The deserialized modbus frame.</returns>
        /// <exception cref="ModbusCheckException">Throws an exception if checksum failed.</exception>
        public override object Deserialize(Stream serializationStream)
        {
            T frame = new();
            using HexStream hs = new(serializationStream);
            using Lrc lrcStream = new(hs);
            if (hs.ReadAsc(1) == ":")
            {
                frame.FillFrom(lrcStream, Role == ModbusRoles.Slave ? IModbusSerializable.FrameTypes.Request : IModbusSerializable.FrameTypes.Response);
                int check = hs.ReadByte();
                if (hs.ReadAsc(2) == "\r\n" && lrcStream.ReadLrc == check)
                    return frame;
                else
                    throw new ModbusCheckException();
            }
            throw new ModbusCheckException();
        }

        /// <summary>
        /// Serialize the modbus frame to the serial stream.
        /// </summary>
        /// <param name="serializationStream">The stream where the formatter puts the serialized data. This stream can reference a variety of backing stores (such as files, network, memory, and so on).</param>
        /// <param name="frame">The modbus frame to serialize.</param>
        /// <exception cref="ArgumentException">Throws an exception if frame is not IModbusSerializable type.</exception>
        public override void Serialize(Stream serializationStream, object frame)
        {
            if (frame is IModbusSerializable _frame)
                using (HexStream hs = new(serializationStream))
                using (Lrc lrcStream = new(hs))
                {
                    hs.WriteAsc(":");
                    _frame.WriteTo(lrcStream, Role == ModbusRoles.Slave ? IModbusSerializable.FrameTypes.Response : IModbusSerializable.FrameTypes.Request);
                    hs.WriteByte(lrcStream.WriteLrc);
                    hs.WriteAsc("\r\n");
                }
            else
                throw new ArgumentException("Expecting declared generic type");
        }
    }
}
