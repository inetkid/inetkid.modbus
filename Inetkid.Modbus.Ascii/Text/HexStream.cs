﻿using System.Text;

namespace Inetkid.Modbus.Text
{
    /// <summary>
    /// Read or write to hex encoded ascii stream.
    /// </summary>
    public class HexStream : Stream
    {
        /// <summary>
        /// The underlying stream to write hex encoded stream.
        /// </summary>
        /// <value>Returns the underlying Stream.</value>
        protected Stream BaseStream { get; }

        /// <summary>
        /// Initializes a new instance of the HexStream class from the base stream.
        /// </summary>
        /// <param name="stream">The stream that transport modbus data. This stream can reference a variety of backing stores (such as files, network, memory, and so on).</param>
        public HexStream(Stream stream) : base()
        {
            BaseStream = stream;
        }

        /// <summary>
        /// Gets a value indicating whether the underlying stream supports reading.
        /// </summary>
        /// <value>true if the base stream supports reading; otherwise, false.</value>
        public override bool CanRead => BaseStream.CanRead;

        /// <summary>
        /// Gets a value indicating whether the base stream supports seeking.
        /// </summary>
        /// <value>true if the base stream supports seeking; otherwise, false.</value>
        public override bool CanSeek => BaseStream.CanSeek;

        /// <summary>
        /// Gets a value indicating whether the base stream supports writing.
        /// </summary>
        /// <value>true if the base stream supports writing; otherwise, false.</value>
        public override bool CanWrite => BaseStream.CanWrite;

        /// <summary>
        /// Gets the length in bytes of the current stream.
        /// </summary>
        /// <value>A long value representing the length of the stream in bytes.</value>
        public override long Length => BaseStream.Length;

        /// <summary>
        /// Gets or sets the position within the current stream.
        /// </summary>
        /// <value>The current position within the stream.</value>
        public override long Position
        {
            get => BaseStream.Position / 2;
            set => BaseStream.Position = value * 2;
        }

        /// <summary>
        /// Clears and causes any buffered data to be written to the base stream.
        /// </summary>
        public override void Flush() => BaseStream.Flush();

        /// <summary>
        /// Sets the position within the current stream.
        /// </summary>
        /// <param name="offset">A byte offset relative to the origin parameter.</param>
        /// <param name="origin">A value of type System.IO.SeekOrigin indicating the reference point used to obtain the new position.</param>
        /// <returns>The new position within the current stream.</returns>
        public override long Seek(long offset, SeekOrigin origin) => BaseStream.Seek(offset * 2, origin);

        /// <summary>
        /// Sets the length of the current stream.
        /// </summary>
        /// <param name="value">The desired length of the current stream in bytes.</param>
        public override void SetLength(long value) => BaseStream.SetLength(value * 2);

        /// <summary>
        /// Reads a sequence of bytes from the current stream and advances the position by the number of bytes read.
        /// </summary>
        /// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between offset and (offset + count - 1) replaced by the bytes read from the current source.</param>
        /// <param name="offset">The zero-based byte offset in buffer at which to begin storing the data read from the current stream.</param>
        /// <param name="count">The maximum number of bytes to be read from the current stream.</param>
        /// <returns>The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.</returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            byte[] buffer2 = new byte[count * 2];
            int bytesRead = BaseStream.Read(buffer2, offset * 2, count * 2);
            for (int i = 0; i < bytesRead / 2; i++)
                buffer[i] = byte.Parse(Encoding.ASCII.GetString(buffer2, i * 2, 2), System.Globalization.NumberStyles.HexNumber);
            return bytesRead / 2;
        }

        /// <summary>
        /// Writes a sequence of bytes to the current stream and advances by the number of bytes written.
        /// </summary>
        /// <param name="buffer">An array of bytes. This method copies count bytes from buffer to the current stream.</param>
        /// <param name="offset">The zero-based byte offset in buffer at which to begin copying bytes to the current stream.</param>
        /// <param name="count">The number of bytes to be written to the current stream.</param>
        public override void Write(byte[] buffer, int offset, int count)
        {
            byte[] buffer2 = new byte[count * 2];
            for (int i = 0; i < count; i++)
            {
                byte[] r = Encoding.ASCII.GetBytes(buffer[i].ToString("X2"));
                buffer2[i * 2] = r[0];
                buffer2[i * 2 + 1] = r[1];
            }
            BaseStream.Write(buffer2, offset * 2, count * 2);
        }

        /// <summary>
        /// Writes a sequence of ascii string as it is to the underlying stream and advances the underlying stream by the number of bytes written.
        /// </summary>
        /// <param name="text">An ascii text to be written to underlying stream directly.</param>
        public virtual void WriteAsc(string text) => BaseStream.Write(Encoding.ASCII.GetBytes(text));

        /// <summary>
        /// Read a sequence of ascii string from the underlying stream and advances the underlying stream by the number of bytes read.
        /// </summary>
        /// <param name="length">The desired length of ascii text to be read.</param>
        /// <returns></returns>
        public virtual string ReadAsc(int length)
        {
            byte[] buffer = new byte[length];
            length = BaseStream.Read(buffer);
            return Encoding.ASCII.GetString(buffer, 0, length);
        }
    }
}
