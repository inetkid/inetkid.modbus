﻿namespace Inetkid.IO.Hashing
{
    /// <summary>
    /// Compute the Longitudinal Redundancy Check while reading and writing through the stream
    /// </summary>
    public class Lrc : System.IO.Stream, IDisposable
    {
        /// <summary>
        /// The underlying stream.
        /// </summary>
        /// <value>Return the underlying Stream.</value>
        protected Stream BaseStream { get; }

        /// <summary>
        /// The input stream checksum
        /// </summary>
        /// <value>The UInt16 value of the input stream CRC</value>
        public byte ReadLrc { get; set; }

        /// <summary>
        /// The output stream checksum
        /// </summary>
        /// <value>The UInt16 value of the output stream LRC</value>
        public byte WriteLrc { get; set; }

        /// <summary>
        /// Initializes a new instance of the Crc16 class with backing of an underlying stream.
        /// </summary>
        /// <param name="stream">The underlying stream to read and write through. This stream can reference a variety of backing stores (such as files, network, memory, and so on).</param>
        /// <param name="initialValue">The initial checksum value.</param>
        public Lrc(Stream stream, byte initialValue = 0)
        {
            BaseStream = stream;
            ReadLrc = WriteLrc = initialValue;
        }

        /// <summary>
        /// Gets a value indicating whether the base stream supports reading.
        /// </summary>
        /// <value>true if the base stream supports reading; otherwise, false.</value>
        public override bool CanRead => BaseStream.CanRead;

        /// <summary>
        /// Gets a value indicating whether the base stream supports seeking.
        /// </summary>
        /// <value>false, the stream do not allow seeking</value>
        public override bool CanSeek => false;

        /// <summary>
        /// Gets a value indicating whether the base stream supports writing.
        /// </summary>
        /// <value>true if the base stream supports writing; otherwise, false.</value>
        public override bool CanWrite => BaseStream.CanWrite;

        /// <summary>
        /// Gets the length in bytes of the current stream.
        /// </summary>
        /// <value>A long value representing the length of the stream in bytes.</value>
        public override long Length => BaseStream.Length;

        /// <summary>
        /// Gets the position within the current stream.
        /// </summary>
        /// <value>The current position within the stream.</value>
        /// <exception cref="NotSupportedException">The stream does not allow setting position.</exception>
        /// <remarks>Setting position not supported.</remarks>
        public override long Position
        {
            get => BaseStream.Position;
            set => throw new NotSupportedException("Stream does not support seeking");
        }

        /// <summary>
        /// Clears and causes any buffered data to be written to the base stream.
        /// </summary>
        public override void Flush() => BaseStream.Flush();

        /// <summary>
        /// Seek not supported.
        /// </summary>
        /// <exception cref="NotSupportedException">The stream does not allow seek.</exception>
        public override long Seek(long offset, SeekOrigin origin) => throw new NotSupportedException("Stream does not support seeking");

        /// <summary>
        /// Sets the length of the current stream.
        /// </summary>
        /// <param name="value">The desired length of the current stream in bytes.</param>
        public override void SetLength(long value) => BaseStream.SetLength(value);

        /// <summary>
        /// Compute the checksum value for the specified byte array.
        /// </summary>
        /// <param name="buffer">An input array of bytes to compute checksum</param>
        /// <param name="offset">The zero-based byte offset in buffer at which to begin compute checksum.</param>
        /// <param name="count">The maximum number of bytes to compute checksum.</param>
        /// <param name="initialValue">The initial checksum value.</param>
        /// <returns>The result of checksum computation</returns>
        public byte ComputeHash(byte[] buffer, int offset, int count, byte initialValue = 0) => ComputeHash(buffer.AsSpan().Slice(offset, count), initialValue);

        /// <summary>
        /// Compute the checksum value for the specified byte array.
        /// </summary>
        /// <param name="buffer">An input span of bytes to compute checksum.</param>
        /// <param name="initialValue">The initial checksum value.</param>
        /// <returns>The result of the checksum computation.</returns>
        public virtual byte ComputeHash(ReadOnlySpan<byte> buffer, byte initialValue = 0)
        {
            foreach (byte b in buffer)
                initialValue = (byte)(((int)initialValue - (int)b) & 255);
            return initialValue;
        }

        /// <summary>
        /// Reads a sequence of bytes from the current stream and advances the position by the number of bytes read.
        /// </summary>
        /// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between offset and (offset + count - 1) replaced by the bytes read from the current source.</param>
        /// <param name="offset">The zero-based byte offset in buffer at which to begin storing the data read from the current stream.</param>
        /// <param name="count">The maximum number of bytes to be read from the current stream.</param>
        /// <returns>The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.</returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            count = BaseStream.Read(buffer, offset, count);
            ReadLrc = ComputeHash(buffer, offset, count, ReadLrc);
            return count;
        }

        /// <summary>
        /// Writes a sequence of bytes to the current stream and advances by the number of bytes written.
        /// </summary>
        /// <param name="buffer">An array of bytes. This method copies count bytes from buffer to the current stream.</param>
        /// <param name="count">The zero-based byte offset in buffer at which to begin copying bytes to the current stream.</param>
        /// <param name="offset">The number of bytes to be written to the current stream.</param>
        public override void Write(byte[] buffer, int offset, int count)
        {
            BaseStream.Write(buffer, offset, count);
            WriteLrc = ComputeHash(buffer, offset, count, WriteLrc);
        }
    }
}