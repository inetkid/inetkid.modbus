﻿using System.IO.Ports;
using Inetkid.Modbus.Command;

byte deviceId = 1;
ushort address = 0;
ushort length = 2;

// Initialize and open the serial port COM1
using SerialPort sp = new("COM1") { ReadTimeout = 500, WriteTimeout = 100 };
sp.Open();

// Initialize ModbusCommand to use the modbus RTU protocol
var command = new ModbusCommand().UseRtu(sp);

// Set the request content to read the first 2 holding registers (analog output) 
// and asynchronously execute the command
var result = command.RequestReadHoldingRegisters(deviceId, address, length).Execute().Result;

Console.WriteLine("Value at register 400001 is " + result.Registers[0]);
Console.WriteLine("Value at register 400002 is " + result.Registers[1]);

