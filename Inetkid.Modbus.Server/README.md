# Modbus TCP Extension Library
Modbus server/slave module for Inetkid.Modbus Library.
Target .NET Standard 2.1. Written in C# for Microsoft Visual Studio 2022.

## Installation
```
dotnet add package Inetkid.Modbus.Server
```

## Usage
Refer to README in Inetkid.Modbus.

## License
Copyright 2023 Raymond Tan.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. 
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

