﻿using Inetkid.Modbus.Handler;
using Inetkid.Modbus.Model;
using Inetkid.Modbus.Collection;

namespace Inetkid.Modbus.Server
{
    public class StatefulHandler : IHandler<ModbusFrameModel, ModbusFrameModel>
    {
        const int RegisterSize = ushort.MaxValue + 1; // Max addressable modbus registers

        /// <summary>
        /// Holds the state of Discrete Output
        /// </summary>
        public HollowList<bool> OutputCoils = new(RegisterSize);

        /// <summary>
        /// Holds the state of Discrete Input
        /// </summary>
        public HollowList<bool> InputCoils = new(RegisterSize);

        /// <summary>
        /// Holds the state of Analog Output
        /// </summary>
        public HollowList<ushort> HoldingRegisters = new(RegisterSize);

        /// <summary>
        /// Holds the state of Analog Input
        /// </summary>
        public HollowList<ushort> InputRegisters = new(RegisterSize);

        /// <summary>
        /// Modbus request handler. Override this handle more functions.
        /// </summary>
        /// <param name="request">The response modbus frame.</param>
        /// <returns>The response modbus frame.</returns>
        public virtual ModbusFrameModel HandleRequest(ModbusFrameModel request)
        {
            ModbusFrameModel response = (ModbusFrameModel)request.Clone();
            switch ((ModbusFrameModel.FunctionCodes)request.FunctionCode)
            {
                case ModbusFrameModel.FunctionCodes.ReadOutputs:
                    response.Coils = OutputCoils.GetRange(request.Address, request.Length).ToArray();
                    break;
                case ModbusFrameModel.FunctionCodes.ReadInputs:
                    response.Coils = InputCoils.GetRange(request.Address, request.Length).ToArray();
                    break;
                case ModbusFrameModel.FunctionCodes.ReadHoldingRegisters:
                    response.Registers = HoldingRegisters.GetRange(request.Address, request.Length).ToArray();
                    break;
                case ModbusFrameModel.FunctionCodes.ReadInputRegisters:
                    response.Registers = InputRegisters.GetRange(request.Address, request.Length).ToArray();
                    break;
                case ModbusFrameModel.FunctionCodes.WriteSingleOutput:
                    OutputCoils[request.Address] = request.Coils[0];
                    break;
                case ModbusFrameModel.FunctionCodes.WriteSingleRegister:
                    HoldingRegisters[request.Address] = request.Registers[0];
                    break;
                case ModbusFrameModel.FunctionCodes.WriteMultiOutputs:
                    OutputCoils.SetRange(request.Address, request.Length, request.Coils);
                    break;
                case ModbusFrameModel.FunctionCodes.WriteMultiRegisters:
                    HoldingRegisters.SetRange(request.Address, request.Length, request.Registers);
                    break;
                default:
                    response.FunctionCode = (byte)(request.FunctionCode | 128);
                    response.ErrorCode = (byte)ModbusFrameModel.ErrorCodes.InvalidFunction;
                    break;
            }
            return response;
        }
    }
}