﻿using System.Collections;

namespace Inetkid.Modbus.Collection
{
    /// <summary>
    /// Fixed size ValueType-typed list that can be accessed by index, and elements initiazed to the default value.
    /// </summary>
    /// <typeparam name="T">The type of the contained elements. Must be a ValueType.</typeparam>
    public class HollowList<T> : IList<T> where T : struct
    {
        /// <summary>
        /// Gets the fixed size of contained elements.
        /// </summary>
        /// <value>Returns the Int32 value of container size.</value>
        public int Count { get; }

        /// <summary>
        /// The actual underlying non empty elements.
        /// </summary>
        /// <value>Returns an Int32 the count of non defaulted elements</value>
        public int FilledCount { get => dic.Count; }

        /// <summary>
        /// The underlying dictionary to hold the indexed collection
        /// </summary>
        protected Dictionary<int, T> dic = new Dictionary<int, T>();

        /// <summary>
        /// Initializes a new instance of the collection that has fixed count of elements of default values.
        /// </summary>
        /// <param name="count">The fixed size of the container</param>
        public HollowList(int count) => Count = count;

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to get or set.</param>
        /// <value>The element at the specified index.</value>
        public T this[int index] { get => dic.ContainsKey(index) ? dic[index] : default; set => dic[index] = value; }

        /// <summary>
        /// Indicate if the collection is readonly
        /// </summary>
        /// <value>Always returns false</value>
        public bool IsReadOnly => false;

        /// <summary>
        /// Set all elements to the default value
        /// </summary>
        public void Clear() => dic.Clear();

        /// <summary>
        /// Creates a shallow copy of a range of elements.
        /// </summary>
        /// <param name="index">The zero-based index at which the range starts.</param>
        /// <param name="count">The number of elements in the range.</param>
        /// <returns>A shallow copy of specified range of elements.</returns>
        public HollowList<T> GetRange(int index, int count)
        {
            HollowList<T> newArray = new HollowList<T>(count);
            newArray.dic = new Dictionary<int, T>();
            int ubound = index + count - 1;
            foreach (int k in dic.Keys)
                if (k >= index && k <= ubound)
                    newArray.dic[k - index] = dic[k];
            return newArray;
        }

        /// <summary>
        /// Set the values of the elements in the range specified.
        /// </summary>
        /// <param name="index">The zero-based index at which the range starts.</param>
        /// <param name="count">The number of elements to set.</param>
        /// <param name="values">The values to be set.</param>
        public void SetRange(int index, int count, IEnumerable<T> values)
        {
            int i = 0;
            foreach (var v in values)
                if (i++ < count)
                    dic[index++] = v;
        }

        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="NotImplementedException"></exception>
        void ICollection<T>.Add(T item) => throw new NotImplementedException();

        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        bool ICollection<T>.Contains(T item) => throw new NotImplementedException();

        /// <summary>
        /// Copies the entire collection to a compatible one-dimensional array, starting at the specified index of the target array.
        /// </summary>
        /// <param name="array">The one-dimensional System.Array that is the destination of the elements to be copied.</param>
        /// <param name="arrayIndex">The zero-based index in array at which copying begins.</param>
        void ICollection<T>.CopyTo(T[] array, int arrayIndex)
        {
            foreach (int k in dic.Keys)
                array[arrayIndex + k] = dic[k];
        }

        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        int IList<T>.IndexOf(T item) => throw new NotImplementedException();

        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        /// <exception cref="NotImplementedException"></exception>
        void IList<T>.Insert(int index, T item) => throw new NotImplementedException();

        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        bool ICollection<T>.Remove(T item) => throw new NotImplementedException();

        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="index"></param>
        /// <exception cref="NotImplementedException"></exception>
        void IList<T>.RemoveAt(int index) => throw new NotImplementedException();

        /// <summary>
        /// Returns an enumerator that iterates a collection.
        /// </summary>
        /// <returns>An System.Collections.IEnumerator object that to be used to iterator.</returns>
        IEnumerator IEnumerable.GetEnumerator() => new HollowArrayEnumerator(dic, Count);

        /// <summary>
        /// Returns an enumerator that iterates a collection.
        /// </summary>
        /// <returns>An System.Collections.IEnumerator<typeparamref name="T"/> object that to be used to iterator.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator() => new HollowArrayEnumerator(dic, Count);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">A ValueType, the type to enumerates</typeparam>
        public class HollowArrayEnumerator : IEnumerator<T>
        {
            private int CurrentIndex, Count;
            private readonly Dictionary<int, T> d;

            /// <summary>
            /// 
            /// </summary>
            /// <param name="dic"></param>
            /// <param name="count"></param>
            internal HollowArrayEnumerator(Dictionary<int, T> dic, int count)
            {
                CurrentIndex = 0;
                d = dic;
                Count = count;
            }

            /// <summary>
            /// Gets the element at the current position of the enumerator.
            /// </summary>
            /// <value>The element at the current position of type <typeparamref name="T"/>.</value>
            public T Current => d.ContainsKey(CurrentIndex) ? d[CurrentIndex] : default;

            /// <summary>
            /// Gets the element at the current position of the enumerator.
            /// </summary>
            /// <value>The element at the current position.</value>
            object IEnumerator.Current => Current;

            /// <summary>
            /// Implement IDisposable.Dispose()
            /// </summary>
            void IDisposable.Dispose() { }

            /// <summary>
            /// Advances the enumerator to the next element of the collection.
            /// </summary>
            /// <returns>true if the enumerator had advanced to the next element; false if the enumerator had reached the end of the collection.</returns>
            public bool MoveNext() => ++CurrentIndex < Count;

            /// <summary>
            /// Reset the enumerator position to the first element of the collection
            /// </summary>
            public void Reset() => CurrentIndex = 0;
        }
    }
}
