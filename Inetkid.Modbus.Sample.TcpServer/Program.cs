﻿using System.Net;
using System.Net.Sockets;
using Inetkid.Modbus.Model;
using Inetkid.Modbus.Serialization;
using Inetkid.Modbus.Server;

// Initialize the Tcp server to listen to port 502
TcpListener listener = new(IPAddress.Any, 0);

// StatefulHandler registers value and process the modbus request
StatefulHandler handler = new();

// Flag to stop listening and exit program
bool fStop = false;

// Listen to Ctrl-C
Console.CancelKeyPress += delegate {
    fStop = true;
};

// Start listening to incoming tcp
listener.Start();

// set a value in register
handler.HoldingRegisters[0] = 123;

while (!fStop)
{
    while (listener.Pending())
        listener.AcceptTcpClientAsync().ContinueWith(async task =>
        {
            using TcpClient client = task.Result;
            using NetworkStream stream = client.GetStream();

            // loop until tcp connection idle for 5 minutes
            for (int idleCounter = 0; idleCounter < 3000; idleCounter++)
            {
                while (stream.DataAvailable)
                {
                    idleCounter = 0;
                    // ModbusTcpFormatter<ModbusFrameModel> formatter = new(ModbusTcpFormatter.ModbusRoles.Server);
                    // ModbusFrameModel request = (ModbusFrameModel)formatter.Deserialize(stream);
                    // ModbusFrameModel response = handler.HandleRequest(request);
                    // formatter.Serialize(stream, response);
                }
                await Task.Delay(100);
            }
        });
    Thread.Sleep(31);
}