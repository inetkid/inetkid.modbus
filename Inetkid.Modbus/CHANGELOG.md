## Change Log

### 2024-01-08 Version 1.2.0.0
Move to .NET Standard 2.1

Bug fixes on serialization codes and modbus tcp codes

Move modbus server/slave classes to Inetkid.Modbus.Server nuget package

Addition of unit tests

Addition of sample codes

### 2023-05-17 Version 1.1.0.3
Split the functionality of RTU, ASCII and TCP to 3 separate nuget package to optimize the package size.

Changes to the abstract class ModbusAdapter

Bug fixes

### 2023-04-19 Version 1.0.0.0
Initial releaase