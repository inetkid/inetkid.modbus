﻿namespace Inetkid.Modbus.Adapter
{
    /// <summary>
    /// Base class to build device specific adapter.
    /// </summary>
    public abstract class ModbusDeviceAdapterBase
    {
        /// <summary>
        /// Get or set the unique RTU device address for communicating with the device. Range between 1 to 255, and default to 1.
        /// </summary>
        public byte DeviceId { get; } = 1;

        /// <summary>
        /// Get or set the desired device name.
        /// </summary>
        public string Name { get; set; } = "";

        /// <summary>
        /// Initializes a new instance of this class with a device unique address.
        /// </summary>
        /// <param name="deviceId">The device unique address (1-255).</param>
        public ModbusDeviceAdapterBase(byte deviceId) => DeviceId = deviceId;

        /// <summary>
        /// Initializes a new instance of the ModbusDeviceAdapter class with a device id and a given device name.
        /// </summary>
        /// <param name="deviceId">The device unique address (1-255).</param>
        /// <param name="name">The device given name.</param>
        public ModbusDeviceAdapterBase(byte deviceId, string name) : this(deviceId) => Name = name;

        /// <summary>
        /// Classs to encapsulate the properties of the modbus device.
        /// </summary>
        public struct DeviceProperty
        {
            /// <summary>
            /// Short unique name of device property
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Description of device property.
            /// </summary>
            public string Caption { get; set; }

            /// <summary>
            /// Get or set the value of the device property.
            /// </summary>
            public ValueType Value { get; set; }

            /// <summary>
            /// Human readable text of the properties consist of caption and value.
            /// </summary>
            /// <returns>The human readable text.</returns>
            public override string ToString() => string.Format("{0}: {1}", string.IsNullOrEmpty(Caption) ? Name : Caption, Value);
        }

        /// <summary>
        /// Generic method to query the device inputs and output values.
        /// </summary>
        /// <returns>Asynchronous System.Threading.Tasks.Task while waiting for the device response of the device properties.</returns>
        public virtual Task<IEnumerable<DeviceProperty>> GenericQuery() { return Task.FromResult((IEnumerable<DeviceProperty>)new DeviceProperty[] { }); }

        /// <summary>
        /// Write to device a new device address.
        /// </summary>
        /// <param name="newDeviceId">The new unique device address</param>
        /// <returns>Asynchronous System.Threading.Tasks.Task while waiting for the device response.</returns>
        /// <exception cref="NotImplementedException">Thows an exception if not implemented.</exception>
        public virtual Task SetDeviceId(byte newDeviceId) { throw new NotImplementedException(); }

        /// <summary>
        /// Write to device a new baud rate.
        /// </summary>
        /// <param name="newRate">The new device baud rate in bits per seconds.</param>
        /// <returns>Asynchronous System.Threading.Tasks.Task while waiting for the device response.</returns>
        /// <exception cref="NotImplementedException">Thows an exception if not implemented.</exception>
        public virtual Task SetBaudRate(int newRate) { throw new NotImplementedException(); }

        /// <summary>
        /// Calibrate analog sensor using reference specimen 1.
        /// </summary>
        /// <returns>Asynchronous System.Threading.Tasks.Task while waiting for the device response.</returns>
        /// <exception cref="NotImplementedException">Thows an exception if not implemented.</exception>
        public virtual Task Calibrate1() { throw new NotImplementedException(); }

        /// <summary>
        /// Calibrate analog sensor using reference specimen 2.
        /// </summary>
        /// <returns>Asynchronous System.Threading.Tasks.Task while waiting for the device response.</returns>
        /// <exception cref="NotImplementedException">Thows an exception if not implemented.</exception>
        public virtual Task Calibrate2() { throw new NotImplementedException(); }

        /// <summary>
        /// Turn discrete output On/Off.
        /// </summary>
        /// <param name="channel">The output channel number.</param>
        /// <param name="value">Set state to On/Off.</param>
        /// <returns>Asynchronous System.Threading.Tasks.Task while waiting for the device response.</returns>
        /// <exception cref="NotImplementedException">Thows an exception if not implemented.</exception>
        public virtual Task SetDigitalOutput(ushort channel, bool value) { throw new NotImplementedException(); }

        /// <summary>
        /// Turn 32 channels discrete outputs On/Off.
        /// </summary>
        /// <param name="value">Set state to On/Off, each bits represent value On/Off for corresponding channels.</param>
        /// <returns>Asynchronous System.Threading.Tasks.Task while waiting for the device response.</returns>
        /// <exception cref="NotImplementedException">Thows an exception if not implemented.</exception>
        public virtual Task SetDigitalOutputs(uint value) { throw new NotImplementedException(); }
    }
}
