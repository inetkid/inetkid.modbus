﻿using Inetkid.Modbus.Model;
using Inetkid.Modbus.Serialization;

namespace Inetkid.Modbus.Command
{
    /// <summary>
    /// Describe the command parameters and function Execute() returning the result of type <typeparamref name="TResult"/>.
    /// </summary>
    /// <typeparam name="TResult">The return type of the Execute() function.</typeparam>
    public interface IModbusCommand<TResult>
    {
        /// <summary>
        /// The System.IO.Stream passed to the constructor.
        /// </summary>
        public Stream? Stream { get; set; }

        /// <summary>
        /// Get or set the serializer/deserializer.
        /// </summary>
        public ModbusFormatter? Formatter { get; set; }

        /// <summary>
        /// Get or set the modbus request frame.
        /// </summary>
        public IModbusSerializable? Request { get; set; }

        /// <summary>
        /// Start running the command asychronously.
        /// </summary>
        /// <returns>System.Threading.Tasks.Task that returns <typeparamref name="TResult"/> after completion of asynchronous run.</returns>
        Task<TResult> Execute();
    }
}
