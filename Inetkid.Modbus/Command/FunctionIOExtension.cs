﻿using Inetkid.Modbus.Model;
using Inetkid.Modbus.Serialization;

namespace Inetkid.Modbus.Command
{
    /// <summary>
    /// Provide a helper extension to construct request of modbus input and output functions.
    /// </summary>
    public static class FunctionIOExtension
    {
        /// <summary>
        /// Creates modbus request frame to read range of discrete output (function code 1).
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <param name="deviceId">The unique address of the target device.</param>
        /// <param name="address">The start address of the discrete outputs.</param>
        /// <param name="length">The count of values to request.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<IModbusSerializable> RequestReadOutputs(this IModbusCommand<IModbusSerializable> command, byte deviceId, ushort address, ushort length)
        {
            command.Request = new ModbusFrameModel(deviceId, (byte)ModbusFrameModel.FunctionCodes.ReadOutputs, address, length);
            return command;
        }

        /// <summary>
        /// Creates modbus request frame for read range of discrete inputs (function code 2).
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <param name="deviceId">The unique address of the target device.</param>
        /// <param name="address">The start address of the discrete inputs.</param>
        /// <param name="length">The count of values to request.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<IModbusSerializable> RequestReadInputs(this IModbusCommand<IModbusSerializable> command, byte deviceId, ushort address, ushort length)
        {
            command.Request = new ModbusFrameModel(deviceId, (byte)ModbusFrameModel.FunctionCodes.ReadInputs, address, length);
            return command;
        }

        /// <summary>
        /// Creates modbus request frame to read range of analog outputs (function code 3).
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <param name="deviceId">The unique address of the target device.</param>
        /// <param name="address">The start address of the analog outputs.</param>
        /// <param name="length">The count of values to request.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<IModbusSerializable> RequestReadHoldingRegisters(this IModbusCommand<IModbusSerializable> command, byte deviceId, ushort address, ushort length)
        {
            command.Request = new ModbusFrameModel(deviceId, (byte)ModbusFrameModel.FunctionCodes.ReadHoldingRegisters, address, length);
            return command;
        }

        /// <summary>
        /// Creates modbus request frame to read range of analog inputs (function code 4).
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <param name="deviceId">The unique address of the target device.</param>
        /// <param name="address">The start address of analog inputs.</param>
        /// <param name="length">The count of values to request.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<IModbusSerializable> RequestReadInputRegisters(this IModbusCommand<IModbusSerializable> command, byte deviceId, ushort address, ushort length)
        {
            command.Request = new ModbusFrameModel(deviceId, (byte)ModbusFrameModel.FunctionCodes.ReadInputRegisters, address, length);
            return command;
        }

        /// <summary>
        /// Creates modbus request frame to set single discrete output (function code 5).
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <param name="deviceId">The unique address of the target device.</param>
        /// <param name="address">The start address of discrete output.</param>
        /// <param name="Value">The value to write, in boolean.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<IModbusSerializable> RequestWriteSingleOutput(this IModbusCommand<IModbusSerializable> command, byte deviceId, ushort address, bool value)
        {
            command.Request = new ModbusFrameModel(deviceId, (byte)ModbusFrameModel.FunctionCodes.WriteSingleOutput, address, new bool[] { value });
            return command;
        }

        /// <summary>
        /// Creates modbus request frame to set single analog output (function code 6).
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <param name="deviceId">The unique address of the target device.</param>
        /// <param name="address">The start address of analog output.</param>
        /// <param name="Value">The value to write, in UInt16.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<IModbusSerializable> RequestWriteSingleRegister(this IModbusCommand<IModbusSerializable> command, byte deviceId, ushort address, ushort value)
        {
            command.Request = new ModbusFrameModel(deviceId, (byte)ModbusFrameModel.FunctionCodes.WriteSingleRegister, address, new ushort[] { value });
            return command;
        }

        /// <summary>
        /// Creates modbus request frame to set multiple discrete outputs (function code 15).
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <param name="deviceId">The unique address of the target device.</param>
        /// <param name="address">The start address of discrete outputs.</param>
        /// <param name="Values">The values to write, in array of booleans.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<IModbusSerializable> RequestWriteMultiOutputs(this IModbusCommand<IModbusSerializable> command, byte deviceId, ushort address, bool[] values)
        {
            command.Request = new ModbusFrameModel(deviceId, (byte)ModbusFrameModel.FunctionCodes.WriteMultiOutputs, address, values);
            return command;
        }

        /// <summary>
        /// Creates modbus request frame to set multiple analog outputs (function code 16).
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <param name="deviceId">The unique address of the target device.</param>
        /// <param name="address">The adress of the input or output.</param>
        /// <param name="Values">The values to set, in array of UInt16</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<IModbusSerializable> RequestWriteMultiRegisters(this IModbusCommand<IModbusSerializable> command, byte deviceId, ushort address, ushort[] values)
        {
            command.Request = new ModbusFrameModel(deviceId, (byte)ModbusFrameModel.FunctionCodes.WriteMultiRegisters, address, values);
            return command;
        }
    }
}
