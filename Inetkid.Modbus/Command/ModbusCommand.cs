﻿using Inetkid.Modbus.Serialization;
using Inetkid.Modbus.Model;

namespace Inetkid.Modbus.Command
{
    /// <summary>
    /// Encapsulates and executes a modbus request. Not thread safe. Override this to extend modbus functionalities.
    /// </summary>
    public class ModbusCommand : IModbusCommand<IModbusSerializable>
    {
        /// <summary>
        /// The System.IO.Stream passed to the constructor.
        /// </summary>
        public Stream? Stream { get; set; }

        /// <summary>
        /// Get or set the serializer/deserializer.
        /// </summary>
        public ModbusFormatter? Formatter { get; set; }

        /// <summary>
        /// Get or set the modbus request frame.
        /// </summary>
        public IModbusSerializable? Request { get; set; }

        /// <summary>
        /// Get or set the delay before waiting for request.
        /// </summary>
        public int ResponseDelay { get; set; } = 250;

        /// <summary>
        /// Initializes a new instance of this class.
        /// </summary>
        public ModbusCommand() { }

        /// <summary>
        /// Initializes a new instance of this class with a stream to serialize.
        /// </summary>
        /// <param name="stream">The System.IO.Stream that transport modbus data. This stream can reference a variety of backing stores (such as files, network, memory, and so on).</param>
        public ModbusCommand(Stream stream) => Stream = stream;

        /// <summary>
        /// Initializes a new instance of this class with a stream and a serializer.
        /// </summary>
        /// <param name="stream">The System.IO.Stream that transport modbus data. This stream can reference a variety of backing stores (such as files, network, memory, and so on).</param>
        /// <param name="formatter">The serializer to read and write modbus frame to the stream.</param>
        public ModbusCommand(Stream stream, ModbusFormatter? formatter) : this(stream) => Formatter = formatter;

        /// <summary>
        /// Initializes a new instance of this class with a stream, a serializer and the modbus frame.
        /// </summary>
        /// <param name="stream">The System.IO.Stream that transport modbus data. This stream can reference a variety of backing stores (such as files, network, memory, and so on).</param>
        /// <param name="formatter">The serializer to read and write modbus frame to the stream.</param>
        /// <param name="request">The the modbus request frame.</param>
        public ModbusCommand(Stream stream, ModbusFormatter? formatter, IModbusSerializable? request) : this(stream, formatter) => Request = request;

        /// <summary>
        /// Serialize and transmit asynchronously the modbus request to remote device and then wait and read the response.
        /// </summary>
        /// <returns>The deserialized modbus response.</returns>
        /// <exception cref="InvalidOperationException">Throws System.InvalidOperationException if the request frame or formatter or stream is null.</exception>
        public virtual async Task<IModbusSerializable> Execute()
        {
            await SendRequest();
            await Task.Delay(ResponseDelay);
            return await ReadResponse();
        }

        /// <summary>
        /// Serialize and transmit asynchronously the request to the remote device. Useful in Tcp out of order execution.
        /// </summary>
        /// <exception cref="InvalidOperationException">Throws System.InvalidOperationException if the request frame or formatter or stream is null.</exception>
        public virtual Task SendRequest()
        {
            if (Stream == null)
                throw new InvalidOperationException("Stream property is null.");
            else if (Formatter == null)
                throw new InvalidOperationException("Formatter property is null.");
            else if (Request == null)
                throw new InvalidOperationException("Request property is null.");
            else if (Request is IModbusSerializable frame)
            {
                if (frame.TransactionId == 0)
                    frame.TransactionId = (ushort)new Random().Next(ushort.MaxValue);
                Formatter.Serialize(Stream, frame);
                return Stream.FlushAsync();
            }
            else
                throw new InvalidOperationException("Request property is null.");
        }

        /// <summary>
        /// Read response asynchronously from the remote device. Useful in Tcp out of order execution.
        /// </summary>
        /// <exception cref="InvalidOperationException">Throws System.InvalidOperationException if the request frame or formatter or stream is null.</exception>
        public virtual Task<IModbusSerializable> ReadResponse()
        {
            if (Stream == null)
                throw new InvalidOperationException("Stream property is null.");
            else if (Formatter == null)
                throw new InvalidOperationException("Formatter property is null.");
            else
                return new Task<IModbusSerializable>(() =>
                {
                    IModbusSerializable frame = (IModbusSerializable)Formatter.Deserialize(Stream);
                    if (frame.TransactionId == 0 && Request != null)
                        frame.TransactionId = Request.TransactionId;
                    return frame;
                });
        }
    }
}