﻿namespace Inetkid.Modbus.Handler
{
    /// <summary>
    /// An interface to define handler method in request-reponse model.
    /// </summary>
    /// <typeparam name="TRequest">The type of request object.</typeparam>
    /// <typeparam name="TResponse">The type of response object.</typeparam>
    public interface IHandler<TRequest, TResponse>
    {
        /// <summary>
        /// Provide a handler in request-reponse model.
        /// </summary>
        /// <param name="request">The request object of type <typeparamref name="TRequest"/>.</param>
        /// <returns>The returned object of type <typeparamref name="TResponse"/>.</returns>
        public TResponse HandleRequest(TRequest request);
    }
}
