﻿using Inetkid.Modbus.Serialization;
using System.Diagnostics.CodeAnalysis;
using static Inetkid.Modbus.Utility.Binary;

namespace Inetkid.Modbus.Model
{
    /// <summary>
    /// Encapsulates the modbus frame.
    /// </summary>
    public struct ModbusFrameModel : IModbusSerializable, IEquatable<ModbusFrameModel>, ICloneable
    {
        private const ushort ON = 0xff00;
        private const ushort OFF = 0;

        /// <summary>
        /// Initialize Modbus frame with specified DeviceId and Function Code and random TransactionId.
        /// </summary>
        /// <param name="deviceId">Modbus slave address.</param>
        /// <param name="functionCode">Modbus function code.</param>
        public ModbusFrameModel(byte deviceId, byte functionCode)
        {
            TransactionId = 0;
            DeviceId = deviceId;
            FunctionCode = functionCode;
            Address = 0;
            Length = 0;
            Registers = Array.Empty<ushort>();
            Coils = Array.Empty<bool>();
            ErrorCode = 0;
        }

        /// <summary>
        /// Initialize Modbus frame with specified DeviceId and Function Code and random TransactionId.
        /// </summary>
        /// <param name="deviceId">Modbus slave address.</param>
        /// <param name="functionCode">Modbus function code.</param>
        /// <param name="address">The input/output address.</param>
        /// <param name="length">The range of the input/output addresses.</param>
        public ModbusFrameModel(byte deviceId, byte functionCode, ushort address, ushort length) : this(deviceId, functionCode)
        {
            Address = address;
            Length = length;
        }

        /// <summary>
        /// Initialize Modbus frame with specified DeviceId and Function Code and Values.
        /// </summary>
        /// <param name="deviceId">Modbus slave address.</param>
        /// <param name="functionCode">Modbus function code.</param>
        /// <param name="address">The input/output address.</param>
        /// <param name="values">The registers analog values.</param>
        public ModbusFrameModel(byte deviceId, byte functionCode, ushort address, ushort[] values) : this(deviceId, functionCode, address, (ushort)values.Length)
        {
            Registers = values;
        }

        /// <summary>
        /// Initialize Modbus frame with specified DeviceId and Function Code and random TransactionId.
        /// </summary>
        /// <param name="deviceId">Modbus slave address.</param>
        /// <param name="functionCode">Modbus function code.</param>
        /// <param name="address">The input/output address.</param>
        /// <param name="values">The coils discrete values.</param>
        public ModbusFrameModel(byte deviceId, byte functionCode, ushort address, bool[] values) : this(deviceId, functionCode, address, (ushort)values.Length)
        {
            Coils = values;
        }

        /// <summary>
        /// Modbus TCP Header Transaction Id.
        /// </summary>
        public ushort TransactionId { get; set; } = 0;

        /// <summary>
        /// Gets or sets the modbus device unique slave address.
        /// </summary>
        public byte DeviceId { get; set; }

        /// <summary>
        /// Gets or sets the modbus function code.
        /// </summary>
        public byte FunctionCode { get; set; }

        /// <summary>
        /// Gets or sets the address of the input or output registers.
        /// </summary>
        public ushort Address { get; set; }

        /// <summary>
        /// Gets or sets the range from the input/output address to read or write.
        /// </summary>
        public ushort Length { get; set; }

        /// <summary>
        /// Gets or sets the length of analog inputs/outputs corresponding to the address.
        /// </summary>
        public ushort[] Registers { get; set; }

        /// <summary>
        /// Gets or sets the array of discrete inputs/outputs corresponding to the address.
        /// </summary>
        public bool[] Coils { get; set; }

        /// <summary>
        /// Gets or sets the error code of the modbus response.
        /// </summary>
        public byte ErrorCode { get; set; }

        /// <summary>
        /// Supported modbus function codes.
        /// </summary>
        public enum FunctionCodes : byte
        {
            ReadOutputs = 1,
            ReadInputs = 2,
            ReadHoldingRegisters = 3,
            ReadInputRegisters = 4,
            WriteSingleOutput = 5,
            WriteSingleRegister = 6,
            WriteMultiOutputs = 15,
            WriteMultiRegisters = 16,
            ErrorReadOutputs = 129,
            ErrorReadInputs = 130,
            ErrorReadHoldingRegisters = 131,
            ErrorReadInputRegisters = 132,
            ErrorWriteSingleOutput = 133,
            ErrorWriteSingleRegister = 134,
            ErrorWriteMultiOutputs = 143,
            ErrorWriteMultiRegisters = 144,
        }

        /// <summary>
        /// Supported modbus error codes.
        /// </summary>
        public enum ErrorCodes : byte
        {
            InvalidFunction = 1,
            InvalidAddress = 2,
            InvalidValue = 3,
            SlaveError = 4,
            SlaveTimeout = 5,
            SlaveBusy = 6,
            DiagnosticError = 7,
            ParityError = 8,
            GatewayError = 10,
            GatewayDeviceError = 11,
        }

        /// <summary>
        /// Called by formatter to deserialize modbus stream and fills this object.
        /// </summary>
        /// <param name="stream">The input stream to deserialize modbus data.</param>
        /// <param name="frameType">Indicate that this is either request or response frame.</param>
        /// <exception cref="ModbusFunctionCodeException">Thrown if the stream contains an unsupported function code.</exception>
        public void FillFrom(Stream stream, IModbusSerializable.FrameTypes frameType)
        {
            DeviceId = (byte)stream.ReadByte();
            FunctionCode = (byte)stream.ReadByte();
            FunctionCodes fc = (FunctionCodes)FunctionCode;
            switch (fc)
            {
                case FunctionCodes.ReadOutputs:
                case FunctionCodes.ReadInputs:
                case FunctionCodes.ReadHoldingRegisters:
                case FunctionCodes.ReadInputRegisters:
                    if (frameType == IModbusSerializable.FrameTypes.Request)
                    {
                        byte[] buf1 = new byte[4];
                        if (stream.Read(buf1) < buf1.Length)
                            throw new EndOfStreamException();

                        ushort[] vals1 = ReadUInt16BigEndian(buf1);
                        Address = vals1[0];
                        Length = vals1[1];
                    }
                    else
                    {
                        byte[] buf2 = new byte[stream.ReadByte()];
                        if (stream.Read(buf2) < buf2.Length)
                            throw new EndOfStreamException();
                        else if (fc == FunctionCodes.ReadOutputs || fc == FunctionCodes.ReadInputs)
                            Coils = ReadBits(buf2);
                        else
                            Registers = ReadUInt16BigEndian(buf2);
                    }
                    break;
                case FunctionCodes.WriteSingleOutput:
                case FunctionCodes.WriteSingleRegister:
                case FunctionCodes.WriteMultiOutputs:
                case FunctionCodes.WriteMultiRegisters:
                    byte[] buf3 = new byte[4];
                    if (stream.Read(buf3) < buf3.Length)
                        throw new EndOfStreamException();
                    else
                    {
                        ushort[] vals2 = ReadUInt16BigEndian(buf3);
                        Address = vals2[0];
                        Length = 1;
                        if (fc == FunctionCodes.WriteSingleOutput)
                            Coils = new bool[] { vals2[1] != 0 };
                        else if (fc == FunctionCodes.WriteSingleRegister)
                            Registers = new ushort[] { vals2[1] };
                        else
                        {
                            Length = vals2[1];
                            if (frameType == IModbusSerializable.FrameTypes.Request)
                            {
                                byte[] buf4 = new byte[stream.ReadByte()];
                                if (stream.Read(buf4) < buf4.Length)
                                    throw new EndOfStreamException();
                                else if (fc == FunctionCodes.WriteMultiOutputs)
                                    Coils = ReadBits(buf4).Take((int)Length).ToArray();
                                else
                                    Registers = ReadUInt16BigEndian(buf4).Take((int)Length).ToArray();
                            }
                        }
                    }
                    break;
                case FunctionCodes.ErrorReadOutputs:
                case FunctionCodes.ErrorReadInputs:
                case FunctionCodes.ErrorReadHoldingRegisters:
                case FunctionCodes.ErrorReadInputRegisters:
                case FunctionCodes.ErrorWriteSingleOutput:
                case FunctionCodes.ErrorWriteSingleRegister:
                case FunctionCodes.ErrorWriteMultiOutputs:
                case FunctionCodes.ErrorWriteMultiRegisters:
                    ErrorCode = (byte)stream.ReadByte();
                    break;
                default:
                    throw new ModbusFunctionCodeException();
            }
        }

        /// <summary>
        /// Called by formatter to serialize this object into the modbus stream.
        /// </summary>
        /// <param name="stream">The destination stream of the serialized data.</param>
        /// <param name="frameType">Indicate that this is either request or response frame.</param>
        /// <exception cref="ModbusFunctionCodeException">Thrown if this object contains an unsupported function code.</exception>
        public readonly void WriteTo(Stream stream, IModbusSerializable.FrameTypes frameType)
        {
            stream.WriteByte(DeviceId);
            stream.WriteByte(FunctionCode);
            FunctionCodes fc = (FunctionCodes)FunctionCode;
            switch (fc)
            {
                case FunctionCodes.ReadOutputs:
                case FunctionCodes.ReadInputs:
                case FunctionCodes.ReadHoldingRegisters:
                case FunctionCodes.ReadInputRegisters:
                    if (frameType == IModbusSerializable.FrameTypes.Request)
                    {
                        stream.Write(GetBytesBigEndian(Address));
                        stream.Write(GetBytesBigEndian(Length));
                    }
                    else
                    {
                        byte[] buf = (fc == FunctionCodes.ReadOutputs || fc == FunctionCodes.ReadInputs) ? GetBytes(Coils) : GetBytesBigEndian(Registers);
                        stream.WriteByte((byte)buf.Length);
                        stream.Write(buf);
                    }
                    break;
                case FunctionCodes.WriteSingleOutput:
                case FunctionCodes.WriteSingleRegister:
                    if (fc == FunctionCodes.WriteSingleOutput ? Coils.Length != 1 : Registers.Length != 1)
                        throw new ArgumentOutOfRangeException("Invalid output length of " + (fc == FunctionCodes.WriteSingleOutput ? Coils.Length : Registers.Length));
                    stream.Write(GetBytesBigEndian(Address));
                    stream.Write(fc == FunctionCodes.WriteSingleRegister ? GetBytesBigEndian(Registers[0]) : Coils[0] ? GetBytesBigEndian(ON) : GetBytesBigEndian(OFF));
                    break;
                case FunctionCodes.WriteMultiOutputs:
                case FunctionCodes.WriteMultiRegisters:
                    stream.Write(GetBytesBigEndian(Address));
                    stream.Write(GetBytesBigEndian(Length));
                    if (frameType == IModbusSerializable.FrameTypes.Request)
                    {
                        if (fc == FunctionCodes.WriteMultiOutputs ? Coils.Length == 0 : Registers.Length == 0)
                            throw new ArgumentOutOfRangeException("Invalid empty output.");
                        byte[] buf = (fc == FunctionCodes.WriteMultiOutputs) ? GetBytes(Coils) : GetBytesBigEndian(Registers);
                        stream.WriteByte((byte)buf.Length);
                        stream.Write(buf);
                    }
                    break;
                case FunctionCodes.ErrorReadOutputs:
                case FunctionCodes.ErrorReadInputs:
                case FunctionCodes.ErrorReadHoldingRegisters:
                case FunctionCodes.ErrorReadInputRegisters:
                case FunctionCodes.ErrorWriteSingleOutput:
                case FunctionCodes.ErrorWriteSingleRegister:
                case FunctionCodes.ErrorWriteMultiOutputs:
                case FunctionCodes.ErrorWriteMultiRegisters:
                    stream.WriteByte(ErrorCode);
                    break;
                default:
                    throw new ModbusFunctionCodeException();
            }
        }

        /// <summary>
        /// Creates a new instance of a ModbusFrameModel with the same property values as current object
        /// </summary>
        /// <returns>A new instance of a ModbusFrameModel</returns>
        public object Clone()
        {
            return new ModbusFrameModel()
            {
                TransactionId = TransactionId,
                DeviceId = DeviceId,
                FunctionCode = FunctionCode,
                Address = Address,
                Length = Length,
                ErrorCode = ErrorCode,
                Coils = (bool[])Coils.Clone(),
                Registers = (ushort[])Registers.Clone(),
            };
        }

        public readonly bool Equals(ModbusFrameModel target) => this.DeviceId == target.DeviceId && this.ErrorCode == target.ErrorCode && this.TransactionId == target.TransactionId &&
                    this.FunctionCode == target.FunctionCode && this.Address == target.Address && this.Length == target.Length &&
                    this.Coils.SequenceEqual(target.Coils) && this.Registers.SequenceEqual(target.Registers);

        public readonly override bool Equals([NotNullWhen(true)] object? obj) => obj is ModbusFrameModel target && Equals(target);

        public readonly override int GetHashCode() => this.DeviceId.GetHashCode() & this.ErrorCode.GetHashCode() & this.TransactionId &
            this.FunctionCode.GetHashCode() & this.Address.GetHashCode() & this.Length.GetHashCode() &
            this.Coils.Aggregate(0, (s, v) => s << 1 | (v ? 1 : 0)) & this.Registers.Aggregate(0, (s, v) => s << 1 | v);

        public static bool operator ==(ModbusFrameModel a, ModbusFrameModel b) => a.Equals(b);
        public static bool operator !=(ModbusFrameModel a, ModbusFrameModel b) => !a.Equals(b);

    }
}
