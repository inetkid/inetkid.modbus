﻿using System.Runtime.Serialization;

namespace Inetkid.Modbus.Serialization
{
    /// <summary>
    /// Base class for modbus formatters
    /// </summary>
    public abstract class ModbusFormatter : IFormatter
    {
        /// <summary>
        /// Not used
        /// </summary>
        SerializationBinder? IFormatter.Binder { get; set; } = null;

        /// <summary>
        /// Not used
        /// </summary>
        StreamingContext IFormatter.Context { get; set; }

        /// <summary>
        /// Not used
        /// </summary>
        ISurrogateSelector? IFormatter.SurrogateSelector { get; set; } = null;

        /// <summary>
        /// Override this to implement modbus deserialization functionality.
        /// </summary>
        /// <param name="serializationStream">The System.IO.Stream to deserialize.</param>
        /// <returns>The deserialized modbus frame.</returns>
        public abstract object Deserialize(Stream serializationStream);

        /// <summary>
        /// Override this to implement modbus serialization.
        /// </summary>
        /// <param name="serializationStream">The System.IO.Stream where the formatter puts the serialized data. This stream can reference a variety of backing stores (such as files, network, memory, and so on).</param>
        /// <param name="frame">The modbus frame to be serialize.</param>
        public abstract void Serialize(Stream serializationStream, object frame);
    }
}
