﻿namespace Inetkid.Modbus.Serialization
{
    /// <summary>
    /// Describe the functionality for self-serializable modbus model class.
    /// </summary>
    public interface IModbusSerializable
    {
        /// <summary>
        /// Modbus TCP Header Transaction Id
        /// </summary>
        public ushort TransactionId { get; set; }

        /// <summary>
        /// Indicate to the serializer if the frame is either request or response frame.
        /// </summary>
        public enum FrameTypes { Request, Response }

        /// <summary>
        /// Called by formatter to deserialize modbus stream.
        /// </summary>
        /// <param name="stream">The input stream to deserialize modbus data.</param>
        /// <param name="frameType">Indicate that this is either request or response frame.</param>

        public void FillFrom(Stream stream, FrameTypes frameType);

        /// <summary>
        /// Called by formatter to self serialize into the modbus stream.
        /// </summary>
        /// <param name="stream">The destination stream of the serialized data.</param>
        /// <param name="frameType">Indicate that this is either request or response frame.</param>
        public void WriteTo(Stream stream, FrameTypes frameType);
    }
}
