﻿namespace Inetkid.Modbus.Serialization
{
    //
    // Summary:
    //     The exception that is thrown whenever checksum check failed
    public class ModbusCheckException : IOException
    {
        //
        // Summary:
        //     Initializes a new instance of the ModbusChecksumException class with messsage checksum failed
        public ModbusCheckException() : base("Modbus checksum failed.") { }
        //
        // Summary:
        //     Initializes a new instance of the ModbusChecksumException class with its message
        //     string set to message and its inner exception set to null.
        //
        // Parameters:
        //   message:
        //     A System.String that describes the error. The content of message is intended
        //     to be understood by humans. The caller of this constructor is required to ensure
        //     that this string has been localized for the current system culture.
        public ModbusCheckException(string? message) : base(message) { }
    }
}
