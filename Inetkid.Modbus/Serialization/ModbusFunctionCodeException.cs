﻿namespace Inetkid.Modbus.Serialization
{
    /// <summary>
    /// The exception that is thrown whenever unsupported or invalid function code encountered.
    /// </summary>
    public class ModbusFunctionCodeException : IOException
    {
        /// <summary>
        /// Initializes a new instance of the ModbusFunctionCodeException class with its inner exception set to null.
        /// </summary>
        public ModbusFunctionCodeException() : base("Unsupported or invalid modbus function code.") { }
        /// <summary>
        /// Initializes a new instance of the ModbusFunctionCodeException class with its message string set to message and its inner exception set to null.
        /// </summary>
        /// <param name="message">A System.String that describes the error. The content of message is intended to be understood by humans. The caller of this constructor is required to ensure that this string has been localized for the current system culture.</param>
        public ModbusFunctionCodeException(string? message) : base(message) { }
    }
}