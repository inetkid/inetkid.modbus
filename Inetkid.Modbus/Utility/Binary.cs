﻿using System.Buffers.Binary;

namespace Inetkid.Modbus.Utility
{
    /// <summary>
    /// Utility class to perform binary conversions
    /// </summary>
    public static class Binary
    {
        //private static byte[] TableBitReverse = new byte[] { };

        /// <summary>
        /// Convert array of UInt16 into a span of bytes, as big endian.
        /// </summary>
        /// <param name="values">The span of UInt16 to be converted.</param>
        /// <param name="buffer">The output buffer where the converted array byte to be written, as big endian.</param>
        public static void WriteUInt16BigEndian(ReadOnlySpan<ushort> values, Span<byte> buffer)
        {
            for (int i = 0; i < values.Length; i++)
                BinaryPrimitives.WriteUInt16BigEndian(buffer.Slice(i << 1, 2), values[i]);
        }

        /// <summary>
        /// Read array of UInt16 from a span of bytes, as big endian.
        /// </summary>
        /// <param name="buffer">The input buffer where the array byte to be converted, as big endian.</param>
        /// <returns>The span of bytes converted into the array of UInt16.</returns>
        public static ushort[] ReadUInt16BigEndian(ReadOnlySpan<byte> buffer)
        {
            ushort[] result = new ushort[buffer.Length >> 1];
            for (int i = 0; i < result.Length; i++)
                result[i] = BinaryPrimitives.ReadUInt16BigEndian(buffer.Slice(i << 1, 2));
            return result;
        }

        /// <summary>
        /// Join bits into a span of bytes.
        /// </summary>
        /// <param name="values">The input read only span of bit to be joined.</param>
        /// <param name="buffer">The output buffer where the joined bits is to be written.</param>
        public static void WriteBits(ReadOnlySpan<bool> values, Span<byte> buffer)
        {
            if ((values.Length - 1) / 8 + 1 > buffer.Length)
                throw new ArgumentException("Insufficient output buffer capacity.");
            for (int i = 0; i < values.Length; i++)
                if (values[i])
                    buffer[i >> 3] |= (byte)(128 >> (i & 7));
        }

        /// <summary>
        /// Split array of bits from a span of bytes.
        /// </summary>
        /// <param name="buf">The input buffer where the read only span of byte to be split.</param>
        /// <returns>The result of splitting span of bytes into bits.</returns>
        public static bool[] ReadBits(ReadOnlySpan<byte> buf)
        {
            bool[] arr = new bool[buf.Length * 8];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = (buf[i >> 3] & 128 >> (i & 7)) != 0;
            return arr;
        }

        /// <summary>
        /// Convert array of UInt16 into a array of bytes, as big endian.
        /// </summary>
        /// <param name="values">The array of UInt16 to convert to bytes.</param>
        /// <returns>The array of bytes from array of UInt16, as big endian.</returns>
        public static byte[] GetBytesBigEndian(ushort[] values)
        {
            byte[] buffer = new byte[values.Length * 2];
            WriteUInt16BigEndian(values, buffer);
            return buffer;
        }

        /// <summary>
        /// Join array of bits into a array of bytes.
        /// </summary>
        /// <param name="values">The array of bits to joined.</param>
        /// <returns>The array of bytes from array of bits.</returns>
        public static byte[] GetBytes(bool[] values)
        {
            byte[] buffer = new byte[(values.Length - 1) / 8 + 1];
            WriteBits(values, buffer);
            return buffer;
        }

        /// <summary>
        /// Convert UInt16 into a array of bytes, as big endian.
        /// </summary>
        /// <param name="value">The array of UInt16 to convert to bytes.</param>
        /// <returns>The array of bytes from UInt16, as big endian.</returns>
        public static byte[] GetBytesBigEndian(ushort value) => GetBytesBigEndian(new ushort[] { value });

        /// <summary>
        /// Convert two UInt16 into a single floating point.
        /// </summary>
        /// <param name="high">The high order of UInt16 to convert.</param>
        /// <param name="low">The low order of UInt16 to convert.</param>
        /// <returns>The converted single floating point.</returns>
        public static float ReadSingleBigEndian(ushort low, ushort high)
        {
            //BinaryPrimitives.ReadSingleBigEndian(GetBytesBigEndian(new ushort[] { high, low }));
            Span<byte> x = GetBytesBigEndian(new ushort[] { high, low }).AsSpan();
            if (BitConverter.IsLittleEndian)
                x.Reverse();
            return BitConverter.ToSingle(x); 
        }

        /// <summary>
        /// Convert two UInt16 into a UInt32.
        /// </summary>
        /// <param name="high">The high order of UInt16 to convert.</param>
        /// <param name="low">The low order of UInt16 to convert.</param>
        /// <returns>The converted UInt32.</returns>
        public static uint ReadUInt32BigEndian(ushort low, ushort high) => BinaryPrimitives.ReadUInt32BigEndian(GetBytesBigEndian(new ushort[] { high, low }));

        /// <summary>
        /// Convert a single floating point into two UInt16.
        /// </summary>
        /// <param name="value">The single floating point value to convert.</param>
        /// <param name="high">The high order of UInt16 converted.</param>
        /// <param name="low">The low order of UInt16 converted.</param>
        public static void WriteSingleBigEndian(float value, ref ushort low, ref ushort high)
        {
            //BinaryPrimitives.WriteSingleBigEndian(buff, value);
            byte[] buff = BitConverter.GetBytes(value);
            if(BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            high = BinaryPrimitives.ReadUInt16BigEndian(buff.AsSpan(0, 2));
            low = BinaryPrimitives.ReadUInt16BigEndian(buff.AsSpan(2, 2));
        }

        /// <summary>
        /// Convert a UInt32 into two UInt16.
        /// </summary>
        /// <param name="value">The UInt32 value to convert.</param>
        /// <param name="high">The high order of UInt16 converted.</param>
        /// <param name="low">The low order of UInt16 converted.</param>
        public static void WriteUInt32BigEndian(uint value, ref ushort low, ref ushort high)
        {
            byte[] buff = new byte[4];
            BinaryPrimitives.WriteUInt32BigEndian(buff, value);
            high = BinaryPrimitives.ReadUInt16BigEndian(buff.AsSpan(0, 2));
            low = BinaryPrimitives.ReadUInt16BigEndian(buff.AsSpan(2, 2));
        }
    }
}
