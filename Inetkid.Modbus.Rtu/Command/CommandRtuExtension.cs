﻿using Inetkid.Modbus.Model;
using Inetkid.Modbus.Serialization;
using System.IO.Ports;

namespace Inetkid.Modbus.Command
{
    /// <summary>
    /// Provide a helper extension to initialize serializer for the transport layer.
    /// </summary>
    public static class CommandRtuExtension
    {
        /// <summary>
        /// Set the IModbusCommand.Formatter to serialize for modbus RTU transport.
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<ModbusFrameModel> UseRtu(this IModbusCommand<ModbusFrameModel> command)
        {
            command.Formatter = new ModbusRtuFormatter<ModbusFrameModel>(ModbusRtuFormatter.ModbusRoles.Master);
            return command;
        }

        /// <summary>
        /// Set the IModbusCommand.Stream to SerialPort.BaseSteam and IModbusCommand.Formatter to serialize for modbus RTU transport.
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<ModbusFrameModel> UseRtu(this IModbusCommand<ModbusFrameModel> command, SerialPort port)
        {
            if (!port.IsOpen)
                port.Open();
            command.Stream = new BufferedStream(port.BaseStream);
            return command.UseRtu();
        }
    }
}
