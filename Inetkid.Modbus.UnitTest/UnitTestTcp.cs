﻿using Inetkid.Modbus.Command;
using Inetkid.Modbus.Model;
using Inetkid.Modbus.Serialization;
using Inetkid.Modbus.Utility;

namespace Inetkid.Modbus.UnitTest
{
    [TestClass]
    public class UnitTestTcp : UnitTestBase
    {
        protected ModbusFormatter hostFormatter = new ModbusTcpFormatter<ModbusFrameModel>() { Role = ModbusTcpFormatter.ModbusRoles.Client };
        protected ModbusFormatter deviceFormatter = new ModbusTcpFormatter<ModbusFrameModel>() { Role = ModbusTcpFormatter.ModbusRoles.Server };

        protected override ModbusFrameModel EncodeDecodeRequest(ModbusFrameModel input) => EncodeDecode(hostFormatter, deviceFormatter, input);

        protected override ModbusFrameModel EncodeDecodeResponse(ModbusFrameModel input) => EncodeDecode(deviceFormatter, hostFormatter, input);

        protected override void TestCommand(IModbusCommand<IModbusSerializable> command, byte[] ExpectedResult)
        {
            using MemoryStream ms = new();
            command.Stream = ms;
            command.Formatter = hostFormatter;
            try
            {
                command.Execute().Wait(50);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Span<byte> buf = ms.GetBuffer().AsSpan(0, (int)ms.Length);
            ushort[] header = Binary.ReadUInt16BigEndian(buf[0..6]);
            byte[] payload = buf[6..].ToArray();
            Assert.AreEqual((int)header[2], payload.Length);
            CollectionAssert.AreEqual(payload, ExpectedResult);
        }
    }
}
