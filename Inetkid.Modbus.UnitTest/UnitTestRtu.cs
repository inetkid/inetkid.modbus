﻿using Inetkid.Modbus.Command;
using Inetkid.Modbus.Model;
using Inetkid.Modbus.Serialization;

namespace Inetkid.Modbus.UnitTest
{
    [TestClass]
    public class UnitTestRtu : UnitTestBase
    {
        protected ModbusFormatter hostFormatter = new ModbusRtuFormatter<ModbusFrameModel>() { Role = ModbusRtuFormatter.ModbusRoles.Master };
        protected ModbusFormatter deviceFormatter = new ModbusRtuFormatter<ModbusFrameModel>() { Role = ModbusRtuFormatter.ModbusRoles.Slave };

        protected override ModbusFrameModel EncodeDecodeRequest(ModbusFrameModel input) => EncodeDecode(hostFormatter, deviceFormatter, input);

        protected override ModbusFrameModel EncodeDecodeResponse(ModbusFrameModel input) => EncodeDecode(deviceFormatter, hostFormatter, input);

        protected override void TestCommand(IModbusCommand<IModbusSerializable> command, byte[] ExpectedResult)
        {
            using MemoryStream ms = new();
            command.Stream = ms;
            command.Formatter = hostFormatter;
            try
            {
                command.Execute().Wait(50);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            byte[] payload = ms.GetBuffer().AsSpan(0, (int)ms.Length - 2).ToArray();
            CollectionAssert.AreEqual(payload, ExpectedResult);
        }
    }
}
