using Inetkid.Modbus.Command;
using Inetkid.Modbus.Model;
using Inetkid.Modbus.Serialization;

namespace Inetkid.Modbus.UnitTest
{
    public abstract class UnitTestBase
    {
        [TestMethod]
        public void TestCommandReadCoils()
        {
            TestCommand(
                new ModbusCommand().RequestReadOutputs(1, 0, 2),
                new byte[] { 1, 1, 0, 0, 0, 2 }
            );
        }

        [TestMethod]
        public void TestCommandReadInputs()
        {
            TestCommand(
                new ModbusCommand().RequestReadInputs(1, 0, 2),
                new byte[] { 1, 2, 0, 0, 0, 2 }
            );
        }

        [TestMethod]
        public void TestCommandReadHoldingRegisters()
        {
            TestCommand(
                new ModbusCommand().RequestReadHoldingRegisters(1, 0, 2),
                new byte[] { 1, 3, 0, 0, 0, 2 }
            );
        }

        [TestMethod]
        public void TestCommandReadInputRegisters()
        {
            TestCommand(
                new ModbusCommand().RequestReadInputRegisters(1, 0, 2),
                new byte[] { 1, 4, 0, 0, 0, 2 }
            );
        }

        [TestMethod]
        public void TestCommandWriteSingleCoil()
        {
            TestCommand(
                new ModbusCommand().RequestWriteSingleOutput(1, 0, true),
                new byte[] { 1, 5, 0, 0, 255, 0 }
            );
        }

        [TestMethod]
        public void TestCommandWriteSingleRegister()
        {
            TestCommand(
                new ModbusCommand().RequestWriteSingleRegister(1, 0, 1),
                new byte[] { 1, 6, 0, 0, 0, 1 }
            );
        }

        [TestMethod]
        public void TestCommandWriteMultiCoils()
        {
            TestCommand(
                new ModbusCommand().RequestWriteMultiOutputs(1, 0, new bool[] { true }),
                new byte[] { 1, 15, 0, 0, 0, 1, 1, 128 }
            );
        }

        [TestMethod]
        public void TestCommandWriteMultiRegister()
        {
            TestCommand(
                new ModbusCommand().RequestWriteMultiRegisters(1, 0, new ushort[] { 1 }),
                new byte[] { 1, 16, 0, 0, 0, 1, 2, 0, 1 }
            );
        }

        [TestMethod]
        public void TestEncDecRequestReadCoils() => TestEncodeDecodeRequest(new ModbusFrameModel(1, 1, 1, 1));

        [TestMethod]
        public void TestEncDecRequestReadInputs() => TestEncodeDecodeRequest(new ModbusFrameModel(1, 2, 1, 1));

        [TestMethod]
        public void TestEncDecRequestReadHoldingRegisters() => TestEncodeDecodeRequest(new ModbusFrameModel(1, 3, 1, 1));

        [TestMethod]
        public void TestEncDecRequestReadInputRegisters() => TestEncodeDecodeRequest(new ModbusFrameModel(1, 4, 1, 1));

        [TestMethod]
        public void TestEncDecRequestWriteSingleCoil() => TestEncodeDecodeRequest(new ModbusFrameModel(1, 5, 1, new bool[] { true }));

        [TestMethod]
        public void TestEncDecRequestWriteSingleRegister() => TestEncodeDecodeRequest(new ModbusFrameModel(1, 6, 1, new ushort[] { 1 }));

        [TestMethod]
        public void TestEncDecRequestWriteMultiCoils() => TestEncodeDecodeRequest(new ModbusFrameModel(1, 15, 1, new bool[] { true }));

        [TestMethod]
        public void TestEncDecRequestWriteMultiRegisters() => TestEncodeDecodeRequest(new ModbusFrameModel(1, 16, 1, new ushort[] { 1 }));

        [TestMethod]
        public void TestEncDecResponseReadCoils() => TestEncodeDecodeResponse(new ModbusFrameModel(1, 1, 1, new bool[] { true }));

        [TestMethod]
        public void TestEncDecResponseReadInputs() => TestEncodeDecodeResponse(new ModbusFrameModel(1, 2, 1, new bool[] { true }));

        [TestMethod]
        public void TestEncDecResponseReadHoldingRegisters() => TestEncodeDecodeResponse(new ModbusFrameModel(1, 3, 1, new ushort[] { 1 }));

        [TestMethod]
        public void TestEncDecResponseReadInputRegisters() => TestEncodeDecodeResponse(new ModbusFrameModel(1, 4, 1, new ushort[] { 1 }));

        [TestMethod]
        public void TestEncDecResponseWriteSingleCoil() => TestEncodeDecodeResponse(new ModbusFrameModel(1, 5, 1, new bool[] { true }));

        [TestMethod]
        public void TestEncDecResponseWriteSingleRegister() => TestEncodeDecodeResponse(new ModbusFrameModel(1, 6, 1, new ushort[] { 1 }));

        [TestMethod]
        public void TestEncDecResponseWriteMultiCoils() => TestEncodeDecodeResponse(new ModbusFrameModel(1, 15, 1, 1));

        [TestMethod]
        public void TestEncDecResponseWriteMultiRegisters() => TestEncodeDecodeResponse(new ModbusFrameModel(1, 16, 1, 1));

        protected abstract void TestCommand(IModbusCommand<IModbusSerializable> command, byte[] ExpectedResult);

        protected abstract ModbusFrameModel EncodeDecodeRequest(ModbusFrameModel input);

        protected abstract ModbusFrameModel EncodeDecodeResponse(ModbusFrameModel input);

        protected static ModbusFrameModel EncodeDecode(ModbusFormatter serializer, ModbusFormatter deserializer, ModbusFrameModel input)
        {
            using MemoryStream ms = new();
            serializer.Serialize(ms, input);
            Span<byte> buffer = ms.GetBuffer().AsSpan(0, (int)ms.Length);
            ms.Seek(0, SeekOrigin.Begin);
            return (ModbusFrameModel)deserializer.Deserialize(ms);
        }

        protected void TestEncodeDecodeRequest(ModbusFrameModel frame)
        {
            ModbusFrameModel output = EncodeDecodeRequest(frame);
            Assert.AreEqual(frame, output);
        }
        protected void TestEncodeDecodeResponse(ModbusFrameModel frame)
        {
            ModbusFrameModel output = EncodeDecodeResponse(frame);
            output.Address = frame.Address;
            output.Length = frame.Length;
            output.Coils = output.Coils.Take(frame.Length).ToArray();
            output.Registers = output.Registers.Take(frame.Length).ToArray();
            Assert.AreEqual(frame, output);
        }
    }
}