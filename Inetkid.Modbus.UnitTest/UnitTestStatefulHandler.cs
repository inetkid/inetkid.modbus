﻿using System;
using Inetkid.Modbus.Model;
using Inetkid.Modbus.Server;
using NuGet.Frameworks;

namespace Inetkid.Modbus.UnitTest
{
    [TestClass]
    public class UnitTestStatefulHandler
    {
        StatefulHandler handler = new();

        [TestMethod]
        public void TestReadCoils()
        {
            handler.OutputCoils[0] = true;
            ModbusFrameModel request = new(1, (byte)ModbusFrameModel.FunctionCodes.ReadOutputs, 0, 2);
            ModbusFrameModel response = new(1, (byte)ModbusFrameModel.FunctionCodes.ReadOutputs, 0, new bool[] { true, false });
            ModbusFrameModel _response = handler.HandleRequest(request);
            Assert.AreEqual(_response, response);
        }

        [TestMethod]
        public void TestReadInputs()
        {
            handler.InputCoils[0] = true;
            ModbusFrameModel request = new(1, (byte)ModbusFrameModel.FunctionCodes.ReadInputs, 0, 2);
            ModbusFrameModel response = new(1, (byte)ModbusFrameModel.FunctionCodes.ReadInputs, 0, new bool[] { true, false });
            ModbusFrameModel _response = handler.HandleRequest(request);
            Assert.AreEqual(_response, response);
        }

        [TestMethod]
        public void TestReadHoldingRegisters()
        {
            handler.HoldingRegisters[0] = 1;
            ModbusFrameModel request = new(1, (byte)ModbusFrameModel.FunctionCodes.ReadHoldingRegisters, 0, 2);
            ModbusFrameModel response = new(1, (byte)ModbusFrameModel.FunctionCodes.ReadHoldingRegisters, 0, new ushort[] { 1, 0 });
            ModbusFrameModel _response = handler.HandleRequest(request);
            Assert.AreEqual(_response, response);
        }

        [TestMethod]
        public void TestReadInputRegisters()
        {
            handler.InputRegisters[0] = 1;
            ModbusFrameModel request = new(1, (byte)ModbusFrameModel.FunctionCodes.ReadInputRegisters, 0, 2);
            ModbusFrameModel response = new(1, (byte)ModbusFrameModel.FunctionCodes.ReadInputRegisters, 0, new ushort[] { 1, 0 });
            ModbusFrameModel _response = handler.HandleRequest(request);
            Assert.AreEqual(_response, response);
        }

        [TestMethod]
        public void TestWriteSingleCoil()
        {
            ModbusFrameModel request = new(1, (byte)ModbusFrameModel.FunctionCodes.WriteSingleOutput, 2, new bool[] {true});
            ModbusFrameModel response = new(1, (byte)ModbusFrameModel.FunctionCodes.WriteSingleOutput, 2, new bool[] { true });
            ModbusFrameModel _response = handler.HandleRequest(request);
            Assert.AreEqual(_response, response);
            Assert.AreEqual(handler.OutputCoils[2], true);
        }

        [TestMethod]
        public void TestWriteSingleRegister()
        {
            ModbusFrameModel request = new(1, (byte)ModbusFrameModel.FunctionCodes.WriteSingleRegister, 2, new ushort[] { 7 });
            ModbusFrameModel response = new(1, (byte)ModbusFrameModel.FunctionCodes.WriteSingleRegister, 2, new ushort[] { 7 });
            ModbusFrameModel _response = handler.HandleRequest(request);
            Assert.AreEqual(_response, response);
            Assert.AreEqual(handler.HoldingRegisters[2], 7);
        }

        public void TestWriteMultiOutputs()
        {
            ModbusFrameModel request = new(1, (byte)ModbusFrameModel.FunctionCodes.WriteMultiOutputs, 8, new bool[] { true , false, true });
            ModbusFrameModel response = new(1, (byte)ModbusFrameModel.FunctionCodes.WriteMultiOutputs, 8, new bool[] { true, false, true });
            ModbusFrameModel _response = handler.HandleRequest(request);
            Assert.AreEqual(_response, response);
            Assert.AreEqual(handler.OutputCoils[8], true);
            Assert.AreEqual(handler.OutputCoils[9], false);
            Assert.AreEqual(handler.OutputCoils[10], true);
        }

        public void TestWriteMultiRegisters()
        {
            ModbusFrameModel request = new(1, (byte)ModbusFrameModel.FunctionCodes.WriteMultiRegisters, 8, new ushort[] { 1, 2, 3 });
            ModbusFrameModel response = new(1, (byte)ModbusFrameModel.FunctionCodes.WriteMultiRegisters, 8, new ushort[] { 1, 2, 3 });
            ModbusFrameModel _response = handler.HandleRequest(request);
            Assert.AreEqual(_response, response);
            Assert.AreEqual(handler.HoldingRegisters[8], 1);
            Assert.AreEqual(handler.HoldingRegisters[9], 2);
            Assert.AreEqual(handler.HoldingRegisters[10], 3);
        }
    }
}
