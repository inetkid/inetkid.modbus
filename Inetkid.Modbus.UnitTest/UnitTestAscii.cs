﻿using Inetkid.Modbus.Command;
using Inetkid.Modbus.Model;
using Inetkid.Modbus.Serialization;
using System.Text;

namespace Inetkid.Modbus.UnitTest
{
    [TestClass]
    public class UnitTestAscii : UnitTestBase
    {
        protected ModbusFormatter hostFormatter = new ModbusAsciiFormatter<ModbusFrameModel>() { Role = ModbusAsciiFormatter.ModbusRoles.Master };
        protected ModbusFormatter deviceFormatter = new ModbusAsciiFormatter<ModbusFrameModel>() { Role = ModbusAsciiFormatter.ModbusRoles.Slave };

        protected override ModbusFrameModel EncodeDecodeRequest(ModbusFrameModel input) => EncodeDecode(hostFormatter, deviceFormatter, input);

        protected override ModbusFrameModel EncodeDecodeResponse(ModbusFrameModel input) => EncodeDecode(deviceFormatter, hostFormatter, input);

        protected override void TestCommand(IModbusCommand<IModbusSerializable> command, byte[] ExpectedResult)
        {
            using MemoryStream ms = new ();
            command.Stream = ms;
            command.Formatter = hostFormatter;
            try
            {
                command.Execute().Wait(50);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            byte[] payload = ms.GetBuffer().AsSpan(1, (int)ms.Length - 5).ToArray();
            ExpectedResult = ExpectedResult.SelectMany(x => Encoding.ASCII.GetBytes(x.ToString("X2"))).ToArray();
            CollectionAssert.AreEqual(payload, ExpectedResult);
        }
    }
}
