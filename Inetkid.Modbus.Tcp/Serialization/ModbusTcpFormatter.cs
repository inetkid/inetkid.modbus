﻿using System.Buffers.Binary;
using System.Runtime.Serialization;

namespace Inetkid.Modbus.Serialization
{
    /// <summary>
    /// Base class for Modbus TCP serialization.
    /// </summary>
    public abstract class ModbusTcpFormatter : ModbusFormatter
    {
        /// <summary>
        /// Modbus role as Modbus TCP client or Modbus TCP server
        /// </summary>
        public enum ModbusRoles { Client, Server };

        /// <summary>
        /// Get or set the role as Modbus TCP client or Modbus TCP server.
        /// </summary>
        public virtual ModbusRoles Role { get; set; }
    }

    /// <summary>
    /// Serialize and deserialize modbus frame from Modbus TCP stream.
    /// </summary>
    /// <typeparam name="T">A type that hold structured modbus frame and implements the IModbusSerializable.</typeparam>
    public class ModbusTcpFormatter<T> : ModbusTcpFormatter where T : IModbusSerializable, new()
    {
        /// <summary>
        /// Initialize modbus formatter with the default role of a Modbus TCP client.
        /// </summary>
        public ModbusTcpFormatter() : this(ModbusRoles.Client) { }

        /// <summary>
        /// Initialize modbus formatter with specified role.
        /// </summary>
        /// <param name="role">Specify the role of a tcp client or a tcp server.</param>
        public ModbusTcpFormatter(ModbusRoles role) => Role = role;

        /// <summary>
        /// Deserialize modbus frame from the TCP stream.
        /// </summary>
        /// <param name="serializationStream">The System.IO.Stream to deserialize.</param>
        /// <returns>The deserialized modbus frame of type <typeparamref name="T"/>.</returns>
        /// <exception cref="IOException">Thrown if the read modbus frame from the stream was incomplete.</exception>
        public override object Deserialize(Stream serializationStream)
        {
            Span<byte> head = new(new byte[6]);
            if (serializationStream.Read(head) == 6)
            {
                T frame = new() { TransactionId = BinaryPrimitives.ReadUInt16BigEndian(head[..2]) };
                int len = BinaryPrimitives.ReadUInt16BigEndian(head.Slice(4, 2));
                byte[] buf = new byte[len];
                if (serializationStream.Read(buf) == len)
                {
                    frame.FillFrom(new MemoryStream(buf), Role == ModbusRoles.Server ? IModbusSerializable.FrameTypes.Request : IModbusSerializable.FrameTypes.Response);
                    return frame;
                }
            }
            throw new IOException("Modbus frame incomplete");
        }

        /// <summary>
        /// Serialize the Modbus frame to the TCP stream.
        /// </summary>
        /// <param name="serializationStream">The stream where the formatter puts the serialized data. This stream can reference a variety of backing stores (such as files, network, memory, and so on).</param>
        /// <param name="frame">The modbus frame to serialize.</param>
        /// <exception cref="ArgumentException">Thrown if parameter frame is not of the type <typeparamref name="T"/>.</exception>
        public override void Serialize(Stream serializationStream, object frame)
        {
            using MemoryStream ms = new();
            if (frame is IModbusSerializable _frame)
            {
                ms.Write(new byte[] { 0, 0, 0, 0, 0, 0 });
                _frame.WriteTo(ms, Role == ModbusRoles.Server ? IModbusSerializable.FrameTypes.Response : IModbusSerializable.FrameTypes.Request);
                long l = ms.Length;
                byte[] buf = ms.ToArray();
                BinaryPrimitives.WriteUInt16BigEndian(buf.AsSpan()[..2], _frame.TransactionId);
                BinaryPrimitives.WriteUInt16BigEndian(buf.AsSpan()[4..6], (ushort)(l - 6L));
                serializationStream.Write(buf);
            }
            else
                throw new ArgumentException("Expecting declared generic type");
        }
    }
}
