﻿using Inetkid.Modbus.Model;
using Inetkid.Modbus.Serialization;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Inetkid.Modbus.Command
{
    /// <summary>
    /// Provide a helper extension to initialize serializer for the transport layer.
    /// </summary>
    public static class CommandTcpExtension
    {
        /// <summary>
        /// Set the formatter to serialize for modbus TCP transport.
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<ModbusFrameModel> UseTcp(this IModbusCommand<ModbusFrameModel> command)
        {
            command.Formatter = new ModbusTcpFormatter<ModbusFrameModel>(ModbusTcpFormatter.ModbusRoles.Client);
            return command;
        }

        /// <summary>
        /// Set the IModbusCommand.Stream to TcpClient Stream and IModbusCommand.Formatter to serialize for modbus ASCII transport.
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<ModbusFrameModel> UseTcp(this IModbusCommand<ModbusFrameModel> command, TcpClient tcpClient)
        {
            command.Stream = new BufferedStream(tcpClient.GetStream());
            return command.UseTcp();
        }

        /// <summary>
        /// Tcp connect to specified IPEndPoint, then set the IModbusCommand.Stream to connected Stream and IModbusCommand.Formatter to serialize for modbus ASCII transport.
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<ModbusFrameModel> UseTcp(this IModbusCommand<ModbusFrameModel> command, IPEndPoint endPoint)
        {
            TcpClient tcpClient = new (endPoint);
            return command.UseTcp(tcpClient);
        }

        /// <summary>
        /// Tcp connect to specified IPEndPoint, then set the IModbusCommand.Stream to connected Stream and IModbusCommand.Formatter to serialize for modbus ASCII transport.
        /// </summary>
        /// <param name="command">An object of ModbusCommand.</param>
        /// <returns>This object.</returns>
        public static IModbusCommand<ModbusFrameModel> UseTcp(this IModbusCommand<ModbusFrameModel> command, string hostname, int port)
        {
            TcpClient tcpClient = new (hostname, port);
            return command.UseTcp(tcpClient);
        }

        /// <summary>
        /// Clear the stream for the specified duration.
        /// </summary>
        /// <param name="stream">An object of System.IO.Stream for this operation.</param>
        /// <param name="duration">Amount of time for this operation.</param>
        /// <returns>True if stream is clear.</returns>
        public static Task<bool> WaitSilence(this Stream stream, TimeSpan duration) => new (() =>
        {
                stream.Flush();
                DateTime endTime = DateTime.Now.Add(duration);
                byte[] buffer = new byte[64];
                int bytesRead = 0;
                while (DateTime.Now < endTime)
                    if ((bytesRead = stream.Read(buffer)) > 0)
                        Thread.Sleep(1); // usually 15.6 milliseconds on windows
                return bytesRead == 0;
        });
    }
}
